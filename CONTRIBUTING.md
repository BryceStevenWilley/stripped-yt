# Contributing to Stripped YouTube

Improvements are always welcome.

To contribute to Stripped YouTube, follow these steps:

  1. Create an issue with the appropriate label and describe what you are
     working on.
  2. Fork and clone the repository.
  3. Make your changes.
  4. Push your changes.
  5. Create a merge request.

## Issues

If there is some distracting feature on YouTube that you feel should be hidden,
make an issue with the "suggestion" label.

Report bugs, defects, and other issues using the [issue tracker](https://gitlab.com/johnjago/stripped-yt/issues).

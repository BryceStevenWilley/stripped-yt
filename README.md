# Stripped YouTube

No related videos, no comments, no autoplay, no annotations.

YouTube is designed to keep you hooked for hours. It's time to take back your
time.

This Firefox extension hides comments and related videos on YouTube, allowing
you to stay focused on the video. Without extra content on the side, the video
is centered by default for a better viewing experience.

Why hide the comments? Studies have shown that for the same video on two
different websites, "personal insults were significantly more prevalent on the
YouTube platform." [[1]](http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0093609#s3)

Stripped YouTube also disables autoplay and annotations.

## Download

https://addons.mozilla.org/en-US/firefox/addon/stripped-youtube/

## Issues

Report bugs or request features using the [issue tracker](https://gitlab.com/johnjago/stripped-yt/issues).

## Contribute

Is there a distracting "feature" on YouTube that you would like to remove or
disable? Feel free to submit a merge request. See [CONTRIBUTING.md](https://gitlab.com/johnjago/stripped-yt/blob/master/CONTRIBUTING.md)
for details.

## License

GNU GPLv3

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
